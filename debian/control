Source: cli-common
Section: cli-mono
Priority: optional
Maintainer: Лаборатория 50
Build-Depends: debhelper (>= 10),
 debiandoc-sgml
Standards-Version: 3.9.0
Homepage: https://salsa.debian.org/dotnet-team/cli-common
Vcs-Git: https://gitlab.com/lab50/mono/cli-common.git
Vcs-Browser: https://gitlab.com/lab50/mono/cli-common

Package: cli-common
Architecture: all
Replaces: cli-common-dev (<< 0.5.1)
Depends: perl
Pre-Depends: mono-runtime-common (<< 6.12.0.114-0labft6) | mono-runtime (>= 6.12.0.114-0labft7)
Description: common files between all CLI packages
 This package must be installed if a CLI (Common Language Infrastructure)
 runtime environment is desired.
 .
 It covers useful integration and information for CLI implementations in
 Debian GNU/Linux, including:
  * The CLI policy describes how CLI packages should behave and integrate.
  * A FAQ for package maintainers of CLI/.NET applications.
  * Integration for CLRs (Common Language Runtime):
    + Installing libraries into existing GACs (Global Assembly Cache)

Package: cli-common-dev
Architecture: all
Replaces: cli-common (<< 0.4.0)
Depends: debhelper (>= 9),
 cil-disassembler | mono-utils (>= 4.4~),
 strong-name-tool | mono-devel (>= 4.4~),
 libxml-dom-perl
Provides: dh-sequence-cli
Description: common files for building CLI packages
 This package must be installed if a CLI (Common Language Infrastructure)
 packaging environment is desired.
 .
 It includes debhelper scripts for managing automatic dependency tracking
 between native libraries, CLI libraries and CLI applications:
  * dh_aot to AOT dll libraries
  * dh_clideps to generate cli:Depends information for debian/control
  * dh_makeclilibs to create clilibs files that are needed/used by dh_clideps
  * dh_installcligac to register assemblies to be late installed into a GAC
  * dh_cligacpolicy to create and install the policy files into a GAC
  * dh_clifixperms to fix permissions of files in CLI package build directories
  * dh_clistrip to strip CLI debug symbols from package build directories
